﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Sipol.Test.Automation
{
    public abstract class AutomationTests
    {
        public delegate void AutoTestDelegate();

        private static List<Type> Tests = new List<Type>();
        private static Dictionary<string, Exception> Errors = new Dictionary<string, Exception>();
        private static List<string> Succeses = new List<string>();

        private static void LoadTests(Assembly assembly, string _namespace)
        {
            Type[] classToLoad = GetTypesInNamespace(assembly, _namespace);
            if (classToLoad == null) Console.WriteLine("load tests is null");
            if (classToLoad.Length <= 0) Console.WriteLine("no load tests");

            foreach (Type tp in classToLoad)
            {
                if (tp.IsClass && !tp.IsGenericType && tp.IsSubclassOf(typeof(AutomationTests)))
                {
                    //Console.WriteLine("Load AutomationTests for class {0}", tp.Name);
                    object autotests = Activator.CreateInstance(tp);
                    Tests.Add(tp);
                }

            }
        }

        public static void RunTestsFromNamespace(Assembly assembly, string _namespace)
        {
            Tests = new List<Type>();
            Errors = new Dictionary<string, Exception>();
            Succeses = new List<string>();

            LoadTests(assembly, _namespace);
            foreach (Type tp in Tests)
            {
                Console.WriteLine("------------------------------------");
                Console.WriteLine("Tests [{0}]", tp.Name);
                Console.WriteLine("------------------------------------");
                try
                {
                    AutomationTests tests = (AutomationTests)Activator.CreateInstance(tp);
                    tests.RunTests();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Tests class {0}", tp.Name);
                    Console.WriteLine("Error: {0}", ex.Message);
                }
            }

            Console.WriteLine();
            Console.WriteLine("------------------------------------");
            Console.WriteLine("Summary:");
            Console.WriteLine("Number of tests: {0}", Tests.Count);
            Console.WriteLine(" Succesed tests: {0}", Succeses.Count);
            Console.WriteLine("   Failed tests: {0}", Errors.Count);
            Console.WriteLine("------------------------------------");

        }

        private static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return
              assembly.GetTypes()
                      .Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal))
                      .ToArray();
        }

        protected void RunTest(AutoTestDelegate test)
        {
            string methodName = test.Method.Name;
            string testName = GetTestName(methodName);
            Console.WriteLine("   {0}", methodName);
            try
            {
                test?.Invoke();
                Succeses.Add(testName);
               
                Console.WriteLine("++ {0}", methodName);
            }
            catch (Exception testEx)
            {
                Errors.Add(testName, testEx);
                
                Console.WriteLine("-- {0}", methodName);
                Console.WriteLine();
                Console.WriteLine("ERROR: {0}", testEx.Message);
            }

        }

        private static void ClearLastLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.Write(new string(' ', Console.BufferWidth));
            Console.SetCursorPosition(0, Console.CursorTop - 1);
        }

        protected void RunTest(params AutoTestDelegate[] tests)
        {
            foreach (AutoTestDelegate test in tests)
                RunTest(test);
        }

        private string GetTestName(string className, string methodName)
        {
            return string.Format("{0}.{1}", className, methodName);
        }

        private string GetTestName(string methodName)
        {
            return GetTestName(GetType().Name, methodName);
        }


        public abstract void RunTests();
    }
}