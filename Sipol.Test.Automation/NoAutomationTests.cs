﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.Test.Automation
{
    public abstract class NoAutomationTests
    {
        protected void RunTest(AutomationTests.AutoTestDelegate test)
        {
        }

        protected void RunTest(params AutomationTests.AutoTestDelegate[] tests)
        {
        }


        public abstract void RunTests();

    }
}
